import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './Hatform';
import Shoe from './Shoe';
import NewShoe from './NewShoe';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" >
            <Route index element={<Shoe/>} />
            <Route path="new" element={<NewShoe/>} />
          </Route>
          <Route path="hats" element={<HatList />}></Route>
          <Route path="hats/new" element={<HatForm />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
