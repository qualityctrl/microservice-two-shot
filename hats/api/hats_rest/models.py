from django.db import models
from django.urls import reverse


# Create your models here.
class HatVO(models.Model):
    # all of this is copied from the location model in the Wardrobe.
    # this is the data that we want to grab from Wardrobe section of the app

    closet_name = models.CharField(max_length=100)
    # section_number = models.PositiveSmallIntegerField()
    # shelf_number = models.PositiveSmallIntegerField()

    import_href = models.CharField(max_length=100, unique=True)


class Hat(models.Model):
    # hat model will represent each hat enetered inthe data

    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    hat_url = models.URLField(null=True)

# this will be a foreign key, and will hit the api for wardrobe location

    location = models.ForeignKey(
        HatVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )


class Location(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"

    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")
